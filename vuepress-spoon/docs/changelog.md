---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Changelog

## Version 1.4.0
* **Customizable background** : use a [custom background](customization.md#backgroundimage) on the vertical SPooN. This is useful for using your own logo for example.
* **Adjust Volume** : [control the volume](chatbots.md#volume-control) directly from the SDK or set [a new default](customization.md#audio) in the configuration.
* **New guidelines** : guidelines for integrating your [interactive character in the environment](concepts.md#interactive-character-in-an-environment) and [setting up your platform](guidelines.md#platform-installation)
* **Expressive API** : more expressions and sounds have been added to the [Expressive Say](chatbots.md#expressive-say-say-with-expression) and the [Expression Reaction](chatbots.md#expressive-reaction) APIs.

## Version 1.3.0
* **Expressive API** : adding content for a more Expressive behavior in the [Expressive Say](chatbots.md#expressive-say-say-with-expression) and the [Expression Reaction](chatbots.md#expressive-reaction) APIs.
* **QRCode API** : adding an API to scan [QRCodes](chatbots.md#start-qrcode-scan).
* **Customizable incitations** : you can [customize the incitations in the release.conf](customization.md#incitations-scenario-configuration).
* **Clear Image**: it's now possible to disable the white background when [displaying an image](chatbots.md#display-image).
* **Skill List**: you can use a [list of skills](chatbots.md#list-of-skills) to select an action at random or play different actions sequentially.
* **Previous install config migration**: you can now [migrate your previous configuration](installation.md#upgrading) during the installation process

## Version 1.2.0

*   **Character selection**: you can now choose the character with which the users will interact. See documentation about the [concepts](customization.md#character-selection) behind character selection and how to choose your character using the ["character"](customization.md#character) field in `release.conf` file file..
*   **HideImage API**: you can now hide a displayed image at a specific moment in the interaction, for example between two speeches to display another image. See documentation about the ["HideImage" message](chatbots.md#hide-image).
*   **DisplayVideo API**: you can now display videos in your chatbot. See documentation about the ["DisplayVideo" message](chatbots.md#display-video).
*   **Chatbot start trigger customization**: you can now change the start trigger of your chatbots in the interaction flow customization. This custom trigger will be sent to your chatbot instead of the generic "Spoon_GenericStart" trigger. See documentation about the "startTrigger" field and its usage in the [ChatbotConfiguration documentation](customization.md#chatbotconfiguration).
*   **Interaction flow timeouts customization**: you can now customize the timeouts of each of the steps of the interaction flow. See documentation about the "timeout" field and its usage in the different configurations objects in the [InteractionFlow customization documentation](customization.md#interaction-flows)
*   **DialogFlow chatbot example**: you can check our [DialogFlow chatbot example](chatbots.md#dialogflow-example) to see how to use the different messages.

## Version 1.1.0

*   **InteractionFlow customization**: you can now fully customize the interaction flow of your character, using chatbots. See documentation about the [Interaction Flow concepts](concepts.md#interaction-flows) and the technical details on [how to customize your interaction flow using chatbots](customization.md#interaction-flows). 
*   **OpenQuestion**: you can now ask open questions (without pre-defined answers) in your chatbots, with the possibility to add a keyboard to help the user. See documentation about the ["OpenQuestion" message](chatbots.md#open-question).
*   **Menus**: you can now display menus in your chatbots, using the updated version of the ["Multiple Choice Question" message](chatbots.md#multiple-choice-questions) with visualTemplates.
*   **Connection with Inbenta chatbot engine**: our solution is now compatible with Inbenta chatbot engine. See the documentation on [how to connect your Inbenta chatbot](chatbots.md#inbenta).
*   **Easy version installation**: you can now use our installer to easily install a new version when available. See documentation about the [installation process](installation.md).
