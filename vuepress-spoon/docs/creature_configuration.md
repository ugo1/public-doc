---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Creature configuration

## Components size and position

The interaction proposed by Spoon software heavily relies on the different components (screen, camera, microphone, etc…) of the device on which it is installed. The size and the position of these components are defined in a Hardware configuration file that can be read by Spoon software.

## Screen format and UI differences

Based on the format of the screen (horizontal or vertical), the UI elements is displayed differently on the screen :
*   with an horizontal screen, the face of the Character is displayed on the whole screen and UI contents is displayed on top of the face

![Horizontal Screen Display](https://www.spoon-cloud.com/static/docs/img/ui-horizontal.png "Horizontal Screen Display")

*   with a vertical screen, the face is displayed at the top of the screen and the UI is displayed below the face

![Vertical Screen Display](https://www.spoon-cloud.com/static/docs/img/ui-vertical.png "Vertical Screen Display")

## Loading of the best hardware configuration at boot

You can define multiple hardware configurations based on the expected usage. When Spoon software starts, it loads all the available hardware configurations in order to choose the one that best matches the current resolution of the screen in the OS :
*   loads a vertical or horizontal configuration depending on the resolution
*   loads the config that has the closest settings to the current screen resolution as defined by the OS

::: tip
⇒ In order to use a screen vertically, it is necessary to change the orientation in the display settings of your OS

![Display settings](https://www.spoon-cloud.com/static/docs/img/landscape_settings.png "Display settings")

:::

## Hardware configuration location and format

The hardware configurations are stored at the root of your Spoon installation (by default it will be at `C:/Program Files/SPooN`) in a JSON file (`.json`) - choose the name you want, just make sure you use the proper extension. You create one file per hardware configuration.

### Examples

#### Laptop installed on a desk (horizontal screen, default webcam and microphone)
```json
{
  "vertical": false,
  "screen": {
    "size": {
      "x": 0.38,
      "y": 0.24
    },
    "position": {
      "frameReference": "Center",
      "height": 1.25
    }
  },
  "face": {
    "quality": "High",
    "position": {
      "referential": "Screen",
      "frameReference": "Center",
      "position": {
        "x": 0.0,
        "y": 0.0,
        "z": 0.0
      },
      "orientation": {
        "x": 0.0,
        "y": 0.0,
        "z": 0.0
      }
    }
  },
  "cameras": [
    {
      "type": "Other",
      "position": {
        "referential": "Screen",
        "frameReference": "FromTop",
        "position": {
          "x": 0.0,
          "y": 0.01,
          "z": 0.0
        },
        "orientation": {
          "x": 0.0,
          "y": 0.0,
          "z": 0.0
        }
      }
    }
  ],
  "microphones": [
    {
      "type": "Other",
      "position": {
        "referential": "Screen",
        "frameReference": "FromTop",
        "position": {
          "x": 0.0,
          "y": 0.01,
          "z": 0.0
        },
        "orientation": {
          "x": 0.0,
          "y": 0.0,
          "z": 0.0
        }
      }
    }
  ]
}
```
#### ScreenSpoon (vertical screen, Realsense 3D camera, default microphone)
```json
{
  "vertical": true,
  "screen": {
    "size": {
      "x": 0.53,
      "y": 0.94
    },
    "position": {
      "frameReference": "FromTop",
      "height": 1.73
    }
  },
  "face": {
    "quality": "High",
    "position": {
      "referential": "Screen",
      "frameReference": "Center",
      "position": {
        "x": 0.0,
        "y": 0.0,
        "z": 0.0
      },
      "orientation": {
        "x": 0.0,
        "y": 0.0,
        "z": 0.0
      }
    }
  },
  "cameras": [
    {
      "type": "Realsense_Internal",
      "position": {
        "referential": "Screen",
        "frameReference": "FromTop",
        "position": {
          "x": 0.0,
          "y": 0.10,
          "z": 0.0
        },
        "orientation": {
          "x": 5.0,
          "y": 0.0,
          "z": 0.0
        }
      }
    }
  ],
  "microphones": [
    {
      "type": "Other",
      "position": {
        "referential": "Screen",
        "frameReference": "FromTop",
        "position": {
          "x": 0.0,
          "y": 0.1,
          "z": 0.0
        },
        "orientation": {
          "x": 5.0,
          "y": 0.0,
          "z": 0.0
        }
      }
    }
  ]
}
```

### Format description

The hardware configuration should define the following fields :

#### "vertical"

*   **description**: defines whether or not the configuration is for a vertical screen 
*   **type**: bool
*   **content**:
    *   **true**: the configuration is for a vertical screen
    *   **false**: the configuration is for an horizontal screen

#### "screen"

*   **description**: definition of the screen configuration 
*   **type**: ScreenConfiguration
*   **content**:
```json
"screen": {
    "size": {
        "x": <float>,
        "y": <float>
    },
    "position": {
        "frameReference": <string>,
        "height": <float>
    }
}
```
*   **size**: size of the screen
    *   **x**: width of the screen
        *   float (in meters)
    *   **y**: height of the screen
        *   float (in meters)
*   **position**: position of the screen
    *   **frameReference**: reference point for the position definition (and height)
        *   enum among :
            *   **"FromBottom"** : bottom of the screen
            *   **"Center"**: center of the screen
            *   **"FromTop"**: top of the screen
    *   **height**: height of frameReference with respect to the floor
        *   float (in meters)


#### "face"

*   **description**: definition of the face configurations
*   **type**: FaceConfiguration
*   **content**:
```json
"face": {
    "quality": <string>,
    "position": {
        "referential": <string>,
        "frameReference": <string>,
        "position": {
           "x": <float>,
           "y": <float>,
            "z": <float>
    },
        "orientation": {
            "x": <float>,
            "y": <float>,
            "z": <float>
        }
    }
}
```
*   **quality**: resolution of the 3D mesh used for the face
    *   enum among :
        *   **"High"**: high resolution face (recommended for usual computer)
        *   **"Medium"**:  face with reduced resolution (to use when limited CPU or GPU performances on computer)
*   **position**: position of the face
    *   **referential**: frame used for position definition
        *   enum among :
            *   **"Screen"**: position will be defined with respect to the screen frame
    *   **frameReference**: reference point of the referential frame, used for the position definition
        *   enum among :
            *   **"Center"**: center of the screen
    *   **position**: position of the face with respect to the reference point of the referential frame
        *   Vector3(x,y,z) (in meters)
    *   **orientation**: rotation of the face with respect to the reference point of the referential frame
        *   Vector3(x,y,z) (in degrees)

::: warning
For the moment the position can only be defined to the default position, in the center of the screen (the result will automatically work for both horizontal or vertical screens format)

```json
"position": {
    "referential": "Screen",
    "frameReference": "Center",
    "position": {
        "x": 0.0,
        "y": 0.0,
        "z": 0.0
    },
    "orientation": {
        "x": 0.0,
        "y": 0.0,
        "z": 0.0
    }
}
```
:::

#### "cameras"

*   **description**: definition of the camera configurations
*   **type**: List\<CameraConfiguration\>
*   **content**:
```json
"cameras": [
    {
        "type": <string>,
        "position": {
            "referential": <string>,
            "frameReference": <string>,
            "position": {
                "x": <float>,
                "y": <float>,
                "z": <float>
            },
            "orientation": {
                "x": <float>,
                "y": <float>,
                "z": <float>
            }
        }
    }
]
```

*   **type**: type of the camera
    *   enum among
        *   "Realsense_Internal": when using a Realsense D435 connected via USB
        *   "Other": when using any other camera (integrated webcam or camera connected via USB)
*   **position** : position of the camera
    *   **referential**: frame used for position definition
        *   enum among
            *   **"World"**: position will be defined with respect to the World frame
            *   **"Robot"**: position will be defined with respect to the Robot frame (position of the center of the robot on the floor)
            *   **"Screen"**: position will be defined with respect to the screen frame
    *   **frameReference**: reference point of the referential frame, used for the position definition
        *   enum among
            *   **"FromBottom"**: bottom of the referential frame
            *   **"Center"**: center of the referential frame
            *   **"FromTop"**: top of the referential frame
    *   **position**: position of the face with respect to the reference point of the referential frame
        *   Vector3(x,y,z) (in meters)
    *   **orientation**: rotation of the face with respect to the reference point of the referential frame
        *   Vector3(x,y,z) (in degrees)

::: tip
Even though the configuration includes a list of Cameras, we recommend for the moment to only define one camera configuration.
:::


#### "microphones"

*   **description**: definition of the microphone configurations
*   **type**: List\<MicrophoneConfiguration\>
*   **content**:
```json
"microphones": [
    {
        "type": <string>,
        "position": {
            "referential": <string>,
            "frameReference": <string>,
            "position": {
                "x": <float>,
                "y": <float>,
                "z": <float>
            },
            "orientation": {
                "x": <float>,
                "y": <float>,
                "z": <float>
            }
        }
    }
]
```

*   **type**: type of the microphone
    *   enum among
        *   "Other": default value
*   **position** : position of the microphone
    *   **referential**: frame used for position definition
        *   enum among
            *   **"World"**: position will be defined with respect to the World frame
            *   **"Robot"**: position will be defined with respect to the Robot frame (position of the center of the robot on the floor)
            *   **"Screen"**: position will be defined with respect to the screen frame
    *   **frameReference**: reference point of the referential frame, used for the position definition
        *   enum among
            *   **"FromBottom"**: bottom of the referential frame
            *   **"Center"**: center of the referential frame
            *   **"FromTop"**: top of the referential frame
    *   **position**: position of the face with respect to the reference point of the referential frame
        *   Vector3(x,y,z) (in meters)
    *   **orientation**: rotation of the face with respect to the reference point of the referential frame
        *   Vector3(x,y,z) (degrees)

::: tip
Even though the configuration includes a list of Microphones, we recommend for the moment to only define one microphone configuration.
:::
