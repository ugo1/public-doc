---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Concepts

## Experience

### An Interactive Character
Your smart Character uses interaction codes shared by every big mammals: hearing, touch, speach... These codes are **universal, immediately understandable** and can be used by anyone, from small children to the elderlies, from every culture.

This new way of interacting fosters an emotional and affective relationship with these characters. This, in turn, makes it easier and more natural to **access numerical content** which was underused till now.

![Universal Interactions](https://www.spoon-cloud.com/static/docs/img/concepts-1.png "Universal Interactions")

### Interactive Character & Staff / Customer Journey
Make sure you anchor the use of an interactive character in a global experience, including and making each of the team member shine. The interactive character is part of the staff!

![SPooNy is part of the staff](https://www.spoon-cloud.com/static/docs/img/concepts-2.png "SPooNy is part of the staff")

### Interactive Character in an environment
**Location in regard to the flow**

![Location in regard to the flow](https://www.spoon-cloud.com/static/docs/img/location-in-regard-to-the-flow.png "Location in regard to the flow")

Particular importance must be given to the placement of the platform in relation to traffic flows. Too close would blur the detection with too many users and passage in the interaction zone, too far no users will be detected and the incentive behavior may not be launched. Moreover, this location will also depend on the selected use case.

*Frontal capture*

![Frontal capture](https://www.spoon-cloud.com/static/docs/img/location-in-regard-to-the-flow_frontal-capture.png "Frontal capture")

The user systematically enters into interaction with the character. The character is at the entrance or at the intersection of several flows. Recommended for reception and orientation.

*One-way capture*

![One-way capture](https://www.spoon-cloud.com/static/docs/img/location-in-regard-to-the-flow_one-way-capture.png "One-way capture")

The character proposes interaction in the user's path but does not impose it. It only focuses on people going in one direction. Recommended for reception or queues.

*Two-way capture*

![Two-way capture](https://www.spoon-cloud.com/static/docs/img/location-in-regard-to-the-flow_two-way-capture.png "Two-way capture")

The character proposes an interaction in the user's journey but does not impose it. the user then exits the flow to interact more confidentially with the character. Recommended for waiting and long interactions.

**Signages**

Signage is very important to guide and reassure the user in her/his experience. Indeed, it allows to facilitate :
* visibility & personalization of the device
* understanding & role of the character
  * suggestions of sentences to say to the character
  * indication of placement to be well perceived

![Signages examples](https://www.spoon-cloud.com/static/docs/img/signages-example.png  "Signages examples")

### Interaction Flows
Your character has different interaction zones as pictured below. Depending on where the user is and how he crosses the different zones, its behavior is different.

![Interaction zones](https://www.spoon-cloud.com/static/docs/img/interaction_zones.png  "Interaction zones")

* **Public zone**: The user sees the character, but the character doesn't see the user.
* **Social zone**: The character can see the user and will usually start playing **incitations** dynamically to attract the user's attention and get them to move in the personal zone to start the interaction.
* **Personal zone**: The character is now fully focused on the user. In this zone, the interaction starts and the user can now access the character's services.

The configuration of these zones can be customized to best fit with the installation and the expected behaviour of the character. To do so, refer to the [customization section](customization.md#engagementZonesConfiguration).


The interaction with a smart character can mainly follow two different models of interaction flows, that can be customized using the SDK: 
* Linear interaction flow
* Menu-based interaction flow

:::tip
If you understand the concept behind SPooN's interaction flows and are looking for how to customize a flow, refer to the [customization section](customization.md#interaction-flows).
:::

#### Linear Interaction Flow
With this flow, the character is **proactive** through the interaction and directly proposes services to the user. This flow works well for short interactions (typically **less than 2min**). The flow should include **one or two services** at most.

![Linear Interaction Flow](https://www.spoon-cloud.com/static/docs/img/linear-interaction-flow.png  "Linear Interaction Flow")

#### Menu-based Interaction Flow
This flow relies on a menu allowing the user to select the services they want to access among a list of available services. The user has autonomy as to how they want to run the services. It works well for slightly longer interactions (**between 2 and 5min**). You can connect up to **6 different services** in this interaction flow.

![Menu-based Interaction Flow](https://www.spoon-cloud.com/static/docs/img/menu-interaction-flow.png  "Menu-based Interaction Flow")

The user accesses the services in two different ways:
* using the menu: by following the flow, the user is guided to a menu and can select one of the services - either by touch or by saying the trigger displayed in the menu.
* direct access: each service has a natural language based trigger - using intent matching from the chatbot engine. At any point, if the user speech is matching one of the trigger intents then it will start the associated service.

![Accessing services](https://www.spoon-cloud.com/static/docs/img/interaction-flow-menu-based-or-direct-service-access.png  "Accessing services")

:::tip
* It's good practice that the character introduces the different services to the user.
* It's also good practice to limit the number of services. If the character gives the user too many options, the user ends up feeling lost.
:::

#### Building Interaction Flows
Building an interaction flow on an interactive character relies on **4 pilars**. Each of them is necessary for a rich interaction, and the flow needs to guide the user through them to maximise customer satisfaction.

* **Paying attention**
  You want to show the user that they exist in the eyes of the character. This can be concretized through different feedbacks, animations, or the proactivity of the character on non-verbal inputs (long silences, no eye contact, reflex sentences such as "I made a mistake").
* **Providing a service**
  The character has a job to fulfill as best as it can. You need to focus on a nice flow to get this job done efficiently so that the interaction starts on a positive note with the actual service being provided. Job done!
* **Fostering a relationship**
  Providing this service is key, but don't forget to create a relationship between your user and the character. This relationship is unique and adapts to the user. The behaviour shouldn't be the same for someone who wants a quick answer, someone who isn't sure how to use the character, or someone who is playing with that character.
* **Leave with a good memory**
  At the end of the interaction, the user needs to leave with three take away messages: a good access to the service, a friendly interaction and the sentiment that he shared a unique moment with our character.

![User engagement curve](https://www.spoon-cloud.com/static/docs/img/concepts-3.png "User engagement curve")

# Conclusion
You know what are SPooN interactive characters and key points of its integration in a user journey.

Let's continue with the [Installation guide](installation.md).