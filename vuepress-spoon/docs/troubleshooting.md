---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Troubleshooting

## Procedures

### Reboot

#### Automatic reboot
Once setup, SPooN reboots automatically every day (outside of working hours).

#### Manual reboot
Kill the running release if needed (alt + F4) and reboot the computer via the start menu. 

For a keyboard-less install, use the tactile screen: slide your finger from the **extreme left of the screen**. You now have access to all the running applications and can close the running SPooN release by clicking on the cross sign. 

### Full shutdown
To fully shutdown the computer on a fully tactile installation, keep pressing the **top left corner of the screen** until the pc shutdowns. Otherwise use the start menu.

Don't forget to power off the installation.

### Start
* Check that SPooN is plugged in (both in the network and electricity). 
* Power on the installation.
* Wait for the pc to be fully started.
* If the screen turns off, select the correct HDMI input channel.
* If you're on the windows startup screen and SPooN doesn't start, double click the executable "SPooN-ai.exe"

### Report a problem
If you encounter a problem, please check the following first: 
* restart the installation
* check the internet connection
* check the different sensors (camera, 3d camera, microphone) are unobstructed (dirt, ...)
* check you're using the correct HDMI input 
* check your chatbot is working properly

If the installation doesn't start properly: 
* put it in stand-by mode
* turn it off

### Contact support
* qualify the type and gravity of the incident

<table>
  <tr>
   <td rowspan="3" ><strong>Hardware Issue</strong>
   </td>
   <td><strong>minor</strong>
   </td>
   <td>scratches, minor physical deterioration that don't impact SPooN's usage.
   </td>
  </tr>
  <tr>
   <td><strong>major</strong>
   </td>
   <td>SPooN can still be used, but the usage is impacted.
   </td>
  </tr>
  <tr>
   <td><strong>critical</strong>
   </td>
   <td>The hardware issue is such that SPooN can't be used anymore.
   </td>
  </tr>
  <tr>
   <td rowspan="3" ><strong>Software Issue</strong>
   </td>
   <td><strong>minor</strong>
   </td>
   <td>The use case is not impacted, but the statistics are impacted (SPooNAnalytics).
   </td>
  </tr>
  <tr>
   <td><strong>major</strong>
   </td>
   <td>SPooN can still be used, but the usage is impacted.
   </td>
  </tr>
  <tr>
   <td><strong>critical</strong>
   </td>
   <td>The software issue is such that SPooN can't be used anymore.
   </td>
  </tr>
</table>

 * describe the issue: what was the behavior you expected and what was the actual observed behavior
 * what are the steps to reproduce the issue
 * send the information to <support@spoon.ai>: attach the log file (you can find it at `C:/Program Files/SPooN/developers-1.4.0/release/SpoonLogs.log`)

# French version
You'll find the [troubleshooting in French over here](troubleshooting_fr.md).