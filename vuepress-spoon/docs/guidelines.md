---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Guidelines

## Behavior creation

![Basics of behavior creation](https://www.spoon-cloud.com/static/docs/img/guidelines-1.png "Basics of behavior creation")

### Basics
The creation of a behavior goes through the use of a content (text, image, video ...) and expressions that accompany this content.

Several factors that are important to take into account for fine tuning this content-expression association:

* **The context**
  It is important to nuance the behavior of the character depending on the context. For example, when entering into an interaction, the behavior (its emotion and the message delivered) will not be the same as the one for the response to a form or for an error message...
* **The intensity/importance of the interaction stage**
  You can show the level of importance of the message delivered by the behavior of the character and by the intensity of its reaction (for example when the user asks a question and the character has the answer it can show strong joy or just smile. In another situation, if the character doesn't find the answer, it can pretend to think discreetly or turn around to find the answer)
* **The emotion to share**
  (sad, happy, curious ...) Emotions are essential in the interaction with an interactive character. They give rhythm to the exchanges by creating empathy and focus. Choosing the right emotion at the right time with the right message will reinforce the understanding of the message and the fluidity of the interaction.

### Content guidelines
#### Guidelines for text contents
* **Short text**
  Text contents should be short and understandable. 
* **Personality**
  They must transcribe the character's personality in the vocabulary and the sentences used.
* **Breathing**
  Rather than giving a large amount of information in an answer, integrate "breaths" into it. For example, by breaking up the content into several small pieces associated with animations and character expressions. 
* **Engagement**
  Introduce small questions or wait for inputs at certain moments to further involve the user.

#### Guidelines for visual contents
* **Quality**
  Images submitted must be of good quality and in the same format.
* **Content**
  To avoid repetition it is necessary to ensure that the text in the image is not the same one said by the character (mainly for short sentences) 
* **Videos**
  Video content should not exceed 20 seconds to avoid losing the user's focus.
* **Expressiveness**
  You can use images/gifs ... to illustrate a point or accentuate an emotion/expression

#### Traps to avoid
* **Losing the user**
  Don't use overly long text and remember that a lack of interaction makes for a disappointing experience.
* **Commercial text**
  The character's personality can be erased by a phrasing that is "too commercial", which could lead to a loss of empathy and trust from the user and their disinterest.
* **Techno skeptic**
  Constantly put the team first. The character is not there to replace the team but to support it. This positioning must be felt in the way the services are presented.

### Expression  guidelines

![Expression definition](https://www.spoon-cloud.com/static/docs/img/guidelines-2.png "Expression definition")

#### Guidelines
Using expressions is essential to give more impact to the content. Interacting with the character is also more natural with expressions.

* **Before/after verbal content**
Don't forget to insert expressions around verbal content. This helps with the expressiveness, punctuation and emphasis of the content.

* **During verbal content**
 Use expressions to enhance vocal contents and to explicit the intended emotion.
* **After a user input**
When the character shows an expressive reaction to a user input, it rewards and engages the user.

#### Expressive API
To generate an expression, use the provided high-level API.

Depending on when you want to display an expression, there are two APIs :
* **ExpressiveSay**
  Expression played during a speech consisting of an animation,
* **ExpressiveReaction**
  Expression played when no speaking, consisting of an emosound.

#### Traps to avoid
* **Using sound expressions during a verbal content**
  When the character speaks, don't use an expression that contains sound.  It covers the speech and often makes it hard to understand.
* **Chaining expressions**
Using several expressions in a row is often confusing to the user. It also impacts the rhythm of the interaction. 
* **Repetitions**
  Don't over-use a few expressions to avoid repetitions in the behavior.

## Platform installation
An area of 50 cm around the interaction zone must be free of any object, **a buffer zone**, to avoid that they disturb the user's capture.

![Buffer zone](https://www.spoon-cloud.com/static/docs/img/buffer-zone.png "Buffer zone")

To do so, you have to check the capture zone and if needed make some adjustments :
1. Press the **Y** key on the keyboard to access the *blob debug*

![Depth-map raw mode](https://www.spoon-cloud.com/static/docs/img/depth-map_raw-mode.png "Depth-map raw mode")

2. Press the **U** on the keyboard to access the *filtered mode*

![Depth-map blob filtered mode](https://www.spoon-cloud.com/static/docs/img/depth-map_blob-filtered-mode.png "Depth-map blob filtered mode")

3. Make sure that no shapes other than the user's appear on the screen. If this is the case, move the corresponding object away or [adjust the size of the "blob detection zone"](customization.md#blobdetectionzone) in the release.config file
4. If the ground is visible in the "filtered mode" view, [adjust the hardware configuration file](creature_configuration.md#cameras) to match the configuration of your device so the ground should not appear.