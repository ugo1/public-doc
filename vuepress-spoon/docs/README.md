---
home: false
tagline: 1.4.0
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

<header class="hero">
    <div class="holder">
        <img src="https://www.spoon-cloud.com/static/docs/img/spoon-rodin.jpg" alt="hero"> 
    </div>
    <h1 id="main-title">SPooN's developers</h1> 
</header>

This is SPooN's SDK documentation for version 1.4.0 to help developers to create marvelous experience with SPooN's creature.


Please check the [Changelog](changelog.md) page for details about the changes in this version.

This documentation regroups the following subjects:
* [Concepts](concepts.md): an overview of the main concepts and flows used.
* [Install](installation.md): step by step installation instructions.
* [Getting Started](getting_started.md): if you want to get started quickly.
* [Creature configuration](creature_configuration.md): the configuration based on your creature.
* [Customization](customization.md): the customization of the experience with your creature.
* [Chatbots](chatbots.md): implementing your own behavior using a chatbot.
* [Guidelines](guidelines.md): a set of useful guidelines. 
* [Support](troubleshooting.md): if you want to report an issue for example. 
