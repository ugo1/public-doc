---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Agora

Agora is a knowledge enrichment service.

When a user says a sentence to SPooNy which does not trigger any other service:
 * if SPooNy does not know how to answer this sentence, the user can teach a new answer.
 * if SPooNy was previously tought an anwser, it will deliver the answer.

## Teaching new knowledge

When SPooNy does not know how to answer something that you say, your sentence stays at the bottom of the screen, under SPooNy's mouth. If you click on that text, you will be able to teach some new knowledge to SPooNy.

![Teaching a new answer](https://www.spoon-cloud.com/static/docs/img/agora-teach-new-1.png "Teaching a new answer")

This will guide you through the process of teaching a new sentence to SPooNy. When you teach SPooNy the new answer, make sure you validate it by, once again, **clicking on the text that appears when you speak**. 

Well done, you taught SPooNy something new!

![Teaching a new answer](https://www.spoon-cloud.com/static/docs/img/agora-teach-new-2.png "Teaching a new answer")

## Re-triggering what you taught
To trigger that new knowledge, simply say the sentence you used to teach it. It needs to be exactly the same sentence. 

![Triggering an answer](https://www.spoon-cloud.com/static/docs/img/agora-teach-new-3.png "Triggering an answer")

As you can see on the screenshot, you can either:
 * reinforce an answer (it will appear more often) by saying or selecting *merci / thanks*
 * teach a new anser by saying or selecting *tu dis n'importe quoi / I don't understand what you're saying*
