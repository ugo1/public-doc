#!/bin/bash

DOCS_COMMON='24526606'
DOCS_DEVELOPERS='24526640'
GITLAB_TOKEN=$1

export VERSION_NUMBER="1.4.0"
export RELEASE_TYPE="developers"

export SPOON_INSTALL_PATH="C:/Program Files/SPooN"
export SPOON_RELEASE_PATH="${SPOON_INSTALL_PATH}/${RELEASE_TYPE}-${VERSION_NUMBER}/release"
export SPOON_DATA_PATH="${SPOON_INSTALL_PATH}/${RELEASE_TYPE}-${VERSION_NUMBER}/data"

FILES='agora.md chatbots.md concepts.md creature_configuration.md customization.md installation.md getting_started.md guidelines.md troubleshooting.md troubleshooting_fr.md'
FILE_VERSION="master"
DEV_FILES='README.template changelog.md'

# copy readme and changelog from developers
for i in $DEV_FILES; do
    echo "copying $i"
    code=$(curl -s -w "%{http_code}" -o $i --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$DOCS_DEVELOPERS/repository/files/$i/raw?ref=$FILE_VERSION")
    if [ "$code" != "200" ]; then
        echo "Error: $code"
        exit 1
    fi
    echo ""
done

cp README.template README.md

# copy the other files from common
for i in $FILES; do
    echo "copying $i"
    code=$(curl -s -w "%{http_code}" -o $i --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$DOCS_COMMON/repository/files/$i/raw?ref=$FILE_VERSION")
    if [ "$code" != "200" ]; then
        echo "Error: $code"
        exit 1
    fi
    echo ""
done

# replace the envvars
for file in `find . \( -iname "*.md" -o -iname "*.pp" \)`; do
    echo "substituting vars in ${file}"
    envsubst '${VERSION_NUMBER} ${RELEASE_TYPE} ${SPOON_RELEASE_PATH} ${SPOON_RELEASE_PATH} ${SPOON_DATA_PATH} ${SPOON_INSTALL_PATH}' < "${file}" > "${file}_tmp"
    mv "${file}_tmp" "${file}"
done

git config user.email "ugo@spoon.ai"
git config user.name "Ugo Cupcic"
git remote rm origin && git remote add origin git@gitlab.com:ugo1/public-doc.git
git commit -am "new version"
git push  origin HEAD:$CI_COMMIT_REF_NAME