---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Getting Started

Once you've [installed the release and necessary tools](installation.md), it's now time to start your own SPooNy.

## Running the release
Simply go to the release folder of the installation directory. By default it will be at `C:/Program Files/SPooN/developers-1.4.0/release`. Double click on the `Spoon-ai` executable to start the release.

Once the release is loaded - it can take some time on the first run - you see SPooNy's face. SPooNy is now ready to interact with you!

![SPooNy's first run](https://www.spoon-cloud.com/static/docs/img/spoony-first-run.png "SPooNy's first run")

:::tip
If for some reason you encounter some issue running the release, please send us the content of the logs. You will find them in `C:/Program Files/SPooN/developers-1.4.0/release/SpoonLogs`
:::

## Interacting with SPooNy
In order to interact with SPooNy, it has to see you. So make sure you're in front of your camera! 

:::tip
You should notice when SPooNy sees you as it's always very happy and becomes animated when it spots someone.
:::

As SPooNy sees you, it runs the welcoming scenario and you are presented with a menu that makes it possible to interact with your Dialogflow scenario later on. Since you haven't created or linked your chatbot yet, the menu is empty for now. 

<video controls width="640">
    <source src="https://www.spoon-cloud.com/static/docs/spoony-first-run.mp4"
            type="video/mp4">
    
    Sorry, your browser doesn't support embedded videos.
</video>

## Creating our first chatbot
Let's create our first chatbot to be able to fill in that menu! We will just go through the basics here to get you started quickly. If you want a more in depth overview, then read [the Dialogflow explanation](chatbots.md#dialogflow).

Go to [the Dialogflow console](https://dialogflow.cloud.google.com/) and login / create an account if necessary. 

### Create your agent 

Once logged-in in Dialogflow, click on the create agent button.

![Create Agent](https://www.spoon-cloud.com/static/docs/img/dialogflow-create-agent.png "Create Agent")

Fill in the Agent Name, default language and other information - if you're not sure and are doing this on your own Google Account, then it's safe to create a new Google Project for your agent.

Now that the agent is created, let's import a simple example. 

First go to your agent settings:
![Agent Settings](https://www.spoon-cloud.com/static/docs/img/dialogflow-import-agent-1.png "Agent Settings")

Now click on **Export and Import**:
![Import / Export](https://www.spoon-cloud.com/static/docs/img/dialogflow-import-agent-2.png "Import / Export")

Download the [following exported agent](https://www.spoon-cloud.com/static/release/Test_ChatbotIntegration.zip), click on **Restore from ZIP**, and upload this zip file to populate your new agent with a simple example.
![Restore from ZIP](https://www.spoon-cloud.com/static/docs/img/dialogflow-import-agent-3.png "Restore from ZIP")

Your first agent has been imported. You should see a few Intents in the **Intents section**:
![Successful Import](https://www.spoon-cloud.com/static/docs/img/dialogflow-import-agent-4.png "Successful Import")

### Connect your new agent
Now that your agent has been created, please [refer to the following section to connect it to your own SPooNy](chatbots.md#dialogflow#how-to-link-a-spoon-release-with-dialogflow-chatbots).

## Testing the integration
Now that you've linked your agent from the `release.conf` file (usually at `C:/Program Files/SPooN/developers-1.4.0/release/Conf`), SPooNy's menu isn't empty anymore and accesses your imported chatbot. 

<video controls width="640">
    <source src="https://www.spoon-cloud.com/static/docs/spoon-imported-chatbot.mp4"
            type="video/mp4">
    
    Sorry, your browser doesn't support embedded videos.
</video>

## Conclusion

You've now started SPooNy and connected a first chatbot. To take things further, you can:
  * read more about [the Dialogflow integration](chatbots.md#dialogflow).
  * use [Agora](agora.md) to teach some quick replies to SPooNy.
