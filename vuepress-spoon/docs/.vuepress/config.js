module.exports = {
  dest: 'public',
  title: 'SPooN\'s Developers - 1.4.0',
  description: 'docs for SDK',
  temp: '/tmp/',
  themeConfig: {
    sidebarDepth: 2,
    sidebar: [
      { path: '/changelog', title: 'Changelog'},
      { path: '/concepts', title: 'Concepts'},
      { path: '/installation', title: 'Installation'},
      { path: '/getting_started', title: 'Getting Started'},
      { path: '/creature_configuration', title: 'Creature configuration'},
      { path: '/customization', title: 'Customization'},
      { path: '/chatbots', title: 'Chatbots'},
      { path: '/guidelines', title: 'Guidelines'},
      { path: '/troubleshooting', title: 'Support'}
    ],
    nav: [
      { text: 'SPooN.ai', link: 'https://www.spoon.ai' }
    ]
  }
}
