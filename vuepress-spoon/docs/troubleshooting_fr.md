---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Troubleshooting

## Procédures 

### Redémarrage
#### Redémarrage automatique 
Une fois mis en place, SPooN redémarre automatiquement chaque jour (en dehors des horaires de bureau).

#### Redémarrage manuel 
Si la release est en exécution, killer l’application (alt + F4) et redémarrer le pc via le menu démarrer. 

Pour le cas d'une installation sans clavier avec un écran tactile il est également possible de glisser le doigt à partir du **bord extrême gauche** de l'écran afin d'avoir accès à toutes les applications lancées et de pouvoir ainsi fermer la release SPooN en cliquant sur la croix associée.

### Arrêt complet 
Dans le cadre d'une installation SPooN tactile et sans clavier, quand la release est en exécution, appuyer le **coin haut gauche de l’écran** jusqu'à extinction du pc.
Sinon, eteindre le pc via le menu démarrer.

Mettre le SPooN hors tension.

### Démarrage 

* Vérifier que le SPooN est branché sur le courant et le réseau.
* Mettre le SPooN sous tension
* Attendre le démarrage complet
* Si l’écran s’éteint, changer la source vidéo
* Si l’écran windows apparaît, mais pas l’interface SPooN, lancer l’exécutable "SPooN-ai.exe"

### Déclaration d’incident
En cas de dysfonctionnement, merci de vérifier les points basiques suivants:
* Redémarrer la borne
* Vérifier la bonne connexion internet
* Vérifier que les capteurs caméra et micro ne sont pas obstrués (poussières, saletés, ...)
* Vérifier que l’écran est sur la bonne source HDMI
* Vérifier le bon fonctionnement du chatbot

Si la borne ne redémarre pas correctement :
* mise en stand-by de la borne
* éteindre la borne

### contacter le support
* qualifier le type et niveau d’incident 

<table>
  <tr>
   <td rowspan="3" ><strong>Problème Matériel</strong>
   </td>
   <td><strong>mineur</strong>
   </td>
   <td>rayures, détérioration physique ne gênant pas l’usage de Spoon pour l’utilisateur final
   </td>
  </tr>
  <tr>
   <td><strong>majeur</strong>
   </td>
   <td>n’empêchant pas l’usage de Spoon pour l’utilisateur final mais rendant cet usage anormal
   </td>
  </tr>
  <tr>
   <td><strong>critique</strong>
   </td>
   <td>dommage matériel empêchant l’usage total de Spoon pour l’utilisateur final
   </td>
  </tr>
  <tr>
   <td rowspan="3" ><strong>Problème Logiciel </strong>
   </td>
   <td><strong>mineur </strong>
   </td>
   <td>dysfonctionnement n’empêchant pas l’usage de Spoon pour l’utilisateur final mais impactant les statistiques d’usage (SpoonAnalytics)
   </td>
  </tr>
  <tr>
   <td><strong>majeur</strong>
   </td>
   <td>dysfonctionnement n’empêchant pas l’usage de Spoon mais rendant cet usage anormal
   </td>
  </tr>
  <tr>
   <td><strong>critique</strong>
   </td>
   <td>dysfonctionnement empêchant complètement l’usage de Spoon pour l’utilisateur final
   </td>
  </tr>
</table>

* décrire le symptôme: quel était le comportement attendu, et quel est le comportement observé.
* décrire (si possible) les actions pour reproduire le problème
* envoyer les informations au support <support@spoon.ai>: les fichiers de log sont ici: `C:/Program Files/SPooN/developers-1.4.0/release/SpoonLogs.log`