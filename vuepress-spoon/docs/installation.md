---
home: false
tagline: null
footer:  1.4.0 | Commercial License | Copyright © 2020-present SPooN AI
---

# Installation

## Windows
This release is supported on Windows only.

### Install SPooN 

Download the [spoon_install-1.4.0-developers.exe](https://www.spoon-cloud.com/static/install/developers/1.4.0/spoon_install-1.4.0-developers.exe). By default, everything will be installed in the `C:/Program Files/SPooN/developers-1.4.0` folder, but you can change this in the Wizard. 

#### Licences
##### First install
###### SPooN Licence 
When buying a SPooN licence, you received a **Robot identity file**. Simply select the provided `robot_identity.json` during the install process on the first line of the wizard:

![Select Licences](https://www.spoon-cloud.com/static/docs/img/installer-select-licences.png "Select Licences")

###### Speech Recognition 
<DEVELOPERS_ONLY>You should have been provided a `spoonasr_cred.json` file with your licence.</DEVELOPERS_ONLY>
<SCIENCE_PLAYGROUND_ONLY>There are two options here:
* If you chose SPooN to handle the Google Payments for you, then you should have been provided a `spoonasr_cred.json` file with your licence. 
* If you're handling the Google Payments yourself, then simply follow the **Step 1. Set up a Cloud Console Project** from [this tutorial](https://cloud.google.com/speech-to-text/docs/quickstart-client-libraries). Stop when you've **downloaded the private key as JSON**, and rename the `google_credentials.json` to `spoonasr_cred.json`.</SCIENCE_PLAYGROUND_ONLY>

Select the `spoonasr_cred.json` file on the second line of the wizard.

![Select Licences](https://www.spoon-cloud.com/static/docs/img/installer-select-licences.png "Select Licences")

#### Upgrading

When upgrading from a previous version, you can select your SPooN licence file and your speech recognition file from the previous install. It's in the install folder of the previous version, usually in `C://Program Files/SPooN/developers-1.0.0`. Simply select  `robot_identity.json` and `spoonasr_cred.json` during the install process when asked by the wizard:

![Select Licences](https://www.spoon-cloud.com/static/docs/img/installer-select-licences.png "Select Licences")

If you want to migrate your whole configuration from a previous release (chatbots, etc.) simply select the `Conf` folder from the previous release when asked during the installation process (usually at `C:/Program Files/SPooN/developers-1.4.0/release/Conf`).

#### Redistributables
During the installation process, you will see three times a window that looks similar to the one below. If the redistributables are not already installed, click the *install* button. If the redistributables are already installed, simply click on the *close* button.

![Redistributable already installed](https://www.spoon-cloud.com/static/docs/img/redistributable_already_installed.png "Redistributable already installed")

After the install, if you had to install some of the redistributables, please **restart your machine**.

## Conclusion
You've now successfuly installed the release. Make sure to [contact SPooN](mailto:support@spoon.ai) if something was unclear or didn't work during that installation process.

Let's start SPooNy with the [Getting Started guide](getting_started.md).
